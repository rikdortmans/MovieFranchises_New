# moviefranchises

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

An ASP.NET Core 5 application with EF 5 connected to a local SQL database. The application has basic CRUD functionality for Movies, Characters, and Franchises and a couple of high level functionalities and update functions. The SQL database is build using code-first migrations which can be replicated. The controllers are all documented using Swashbuckle.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Fork project and open using Visual Studio 2019. Run the latest migration to create the local database.

## Usage

Run the project and use Swashbuckle for the functionality.

## Maintainers

[@rikdortmans](https://gitlab.com/rikdortmans)

## Contributing

PRs accepted.

## License

MIT © 2022 Rik Dortmans
