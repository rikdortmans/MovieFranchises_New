﻿using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace MovieFranchises.Models.Context
{
    public class MovieDbContext:DbContext
    {
        
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        { 
            optionsBuilder.UseSqlServer("Data Source=localhost\\SQLEXPRESS;Initial Catalog=MoviesDb_New;Integrated Security=True");
        }

        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(
                new Franchise { Id = 1, Name ="Age of Light", Description="Dungeons & Dragons 5e"},
                new Franchise { Id = 2, Name ="Deathwatch", Description="Warhammer 40k"},
                new Franchise { Id = 3, Name ="Pond of Life", Description="Pathfinder" }
                );

            modelBuilder.Entity<Character>().HasData(
                new Character { Id = 1, Name ="Sognir", Alias = "The Monk", Gender = "Male", Picture= "https://static.wikia.nocookie.net/forgottenrealms/images/7/79/Firbolg-5e.jpg/revision/latest?cb=20180623050202" },
                new Character { Id = 2, Name ="Aunrae-Oyi", Alias = "The Druid", Gender = "Female", Picture= "https://s3.amazonaws.com/files.d20.io/images/45807231/LNQ8zPoA6_Gf8VPaWjTEhg/med.png?1516141022" },
                new Character { Id = 3, Name ="Akim", Alias = "The Gunslinger", Gender = "Male", Picture= "https://s3.amazonaws.com/files.d20.io/images/84614442/5b3FdW_coX5_j5t5IzTPJw/med.png?1561215507" },
                new Character { Id = 4, Name ="Shaxana", Alias = "The Ranger", Gender = "Female", Picture= "https://s3.amazonaws.com/files.d20.io/images/267636873/kZthTiBD9XeRvZD_T7s4OA/med.png?1643399798" },
                new Character { Id = 5, Name ="Zahariel", Alias = "The Librarian", Gender = "Male", Picture= "https://s3.amazonaws.com/files.d20.io/images/250519342/nBiHdE4wJE5hCGgctwBPFg/med.jpg?1634325603" },
                new Character { Id = 6, Name ="Flaen", Alias = "The Vanguard", Gender = "Male", Picture= "https://s3.amazonaws.com/files.d20.io/images/266766963/YPcQ94CBhtMtmtrzAq0mCg/med.jpg?1642966999" }
                );

            modelBuilder.Entity<Movie>().HasData(
                new Movie { Id = 1, Title = "The Ruined City", Director = "Rik", FranchiseId = 3, Genre = "Adventure", ReleaseYear = 2018, Poster = "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786", Trailer = "https://www.youtube.com/watch?v=6Oebyc--Tzc" },
                new Movie { Id = 2, Title = "The Pandomoneum", Director = "Rik", FranchiseId = 1, Genre = "Adventure / War", ReleaseYear = 2019, Poster = "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786", Trailer = "https://www.youtube.com/watch?v=6Oebyc--Tzc" },
                new Movie { Id = 3, Title = "The Citadel", Director = "Daniel", FranchiseId = 1, Genre = "Horror / War", ReleaseYear = 2020, Poster = "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397", Trailer = "https://www.youtube.com/watch?v=I5EFrZZgxh8" },
                new Movie { Id = 4, Title = "The Temple of Time", Director = "Daniel", FranchiseId = 3, Genre = "Detective / Adventure", ReleaseYear = 2022, Poster = "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397", Trailer = "https://www.youtube.com/watch?v=I5EFrZZgxh8" },
                new Movie { Id = 5, Title = "Tyranids", Director = "Tim", FranchiseId = 2, Genre = "War / Horror", ReleaseYear = 2021, Poster = "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931", Trailer = "https://www.youtube.com/watch?v=B9V0bOB8sXQ" },
                new Movie { Id = 6, Title = "Necrons", Director = "Tim", FranchiseId = 2, Genre = "War / Horror", ReleaseYear = 2021, Poster = "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931", Trailer = "https://www.youtube.com/watch?v=B9V0bOB8sXQ" }
                );

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(c => c.Characters)
                .UsingEntity(j => j.HasData(
                    new { CharactersId = 1, MoviesId = 1 },
                    new { CharactersId = 1, MoviesId = 2 },
                    new { CharactersId = 1, MoviesId = 4 },
                    new { CharactersId = 2, MoviesId = 2 },
                    new { CharactersId = 2, MoviesId = 3 },
                    new { CharactersId = 3, MoviesId = 2 },
                    new { CharactersId = 3, MoviesId = 3 },
                    new { CharactersId = 4, MoviesId = 3 },
                    new { CharactersId = 4, MoviesId = 4 },
                    new { CharactersId = 5, MoviesId = 5 },
                    new { CharactersId = 5, MoviesId = 6 },
                    new { CharactersId = 6, MoviesId = 5 },
                    new { CharactersId = 6, MoviesId = 6 }

                    ));
        }
    }
}
