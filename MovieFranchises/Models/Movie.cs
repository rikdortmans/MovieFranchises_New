﻿using System.Collections.Generic;

namespace MovieFranchises.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Poster { get; set; }
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
        // Navigational property
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }
    }
}
