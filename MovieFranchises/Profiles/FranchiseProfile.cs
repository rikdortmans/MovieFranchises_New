﻿using AutoMapper;
using MovieFranchises.DTOs.FranchiseDTOs;
using MovieFranchises.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>().ReverseMap();
            CreateMap<FranchiseUpdateDTO, Franchise>().ReverseMap();
        }
    }
}
