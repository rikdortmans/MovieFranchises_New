﻿using AutoMapper;
using MovieFranchises.DTOs.CharacterDTOs;
using MovieFranchises.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<CharacterCreateDTO, Character>().ReverseMap();
            CreateMap<CharacterUpdateDTO, Character>().ReverseMap();
        }
    }
}
