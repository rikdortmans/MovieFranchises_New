﻿using MovieFranchises.DTOs.MovieDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.DTOs.FranchiseDTOs
{
    public class FranchiseReadMoviesDTO
    {
        public int Id { get; set; }
        public ICollection<MovieReadDTO> MovieReadDTOs { get; set; }
    }
}
