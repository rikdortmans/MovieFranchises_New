﻿using MovieFranchises.DTOs.CharacterDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.DTOs.FranchiseDTOs
{
    public class FranchiseReadCharactersDTO
    {
        public int Id { get; set; }
        public ICollection<CharacterReadDTO> CharacterReadDTOs { get; set; }
    }
}
