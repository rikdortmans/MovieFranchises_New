﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.DTOs.FranchiseDTOs
{
    public class FranchiseUpdateMoviesDTO
    {
        public int Id { get; set; }
        public int[] MovieIds { get; set; }
    }
}
