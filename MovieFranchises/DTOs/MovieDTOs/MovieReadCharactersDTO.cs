﻿using MovieFranchises.DTOs.CharacterDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.DTOs.MovieDTOs
{
    public class MovieReadCharactersDTO
    {
        public int Id { get; set; }
        public ICollection<CharacterReadDTO> CharacterReadDTOs { get; set; }
    }
}
