﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchises.DTOs.MovieDTOs
{
    public class MovieUpdateCharactersDTO
    {
        public int Id { get; set; }
        public int[] CharacterIds { get; set; }
    }
}
