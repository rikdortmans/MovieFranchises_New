﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieFranchises.Migrations
{
    public partial class NewDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Alias = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Genre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Poster = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    CharactersId = table.Column<int>(type: "int", nullable: false),
                    MoviesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.CharactersId, x.MoviesId });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Characters_CharactersId",
                        column: x => x.CharactersId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movies_MoviesId",
                        column: x => x.MoviesId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, "The Monk", "Male", "Sognir", "https://static.wikia.nocookie.net/forgottenrealms/images/7/79/Firbolg-5e.jpg/revision/latest?cb=20180623050202" },
                    { 2, "The Druid", "Female", "Aunrae-Oyi", "https://s3.amazonaws.com/files.d20.io/images/45807231/LNQ8zPoA6_Gf8VPaWjTEhg/med.png?1516141022" },
                    { 3, "The Gunslinger", "Male", "Akim", "https://s3.amazonaws.com/files.d20.io/images/84614442/5b3FdW_coX5_j5t5IzTPJw/med.png?1561215507" },
                    { 4, "The Ranger", "Female", "Shaxana", "https://s3.amazonaws.com/files.d20.io/images/267636873/kZthTiBD9XeRvZD_T7s4OA/med.png?1643399798" },
                    { 5, "The Librarian", "Male", "Zahariel", "https://s3.amazonaws.com/files.d20.io/images/250519342/nBiHdE4wJE5hCGgctwBPFg/med.jpg?1634325603" },
                    { 6, "The Vanguard", "Male", "Flaen", "https://s3.amazonaws.com/files.d20.io/images/266766963/YPcQ94CBhtMtmtrzAq0mCg/med.jpg?1642966999" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Dungeons & Dragons 5e", "Age of Light" },
                    { 2, "Warhammer 40k", "Deathwatch" },
                    { 3, "Pathfinder", "Pond of Life" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Poster", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 2, "Rik", 1, "Adventure / War", "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786", 2019, "The Pandomoneum", "https://www.youtube.com/watch?v=6Oebyc--Tzc" },
                    { 3, "Daniel", 1, "Horror / War", "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397", 2020, "The Citadel", "https://www.youtube.com/watch?v=I5EFrZZgxh8" },
                    { 5, "Tim", 2, "War / Horror", "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931", 2021, "Tyranids", "https://www.youtube.com/watch?v=B9V0bOB8sXQ" },
                    { 6, "Tim", 2, "War / Horror", "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931", 2021, "Necrons", "https://www.youtube.com/watch?v=B9V0bOB8sXQ" },
                    { 1, "Rik", 3, "Adventure", "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786", 2018, "The Ruined City", "https://www.youtube.com/watch?v=6Oebyc--Tzc" },
                    { 4, "Daniel", 3, "Detective / Adventure", "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397", 2022, "The Temple of Time", "https://www.youtube.com/watch?v=I5EFrZZgxh8" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 2, 3 },
                    { 3, 3 },
                    { 4, 3 },
                    { 5, 5 },
                    { 6, 5 },
                    { 5, 6 },
                    { 6, 6 },
                    { 1, 1 },
                    { 1, 4 },
                    { 4, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie",
                column: "MoviesId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
