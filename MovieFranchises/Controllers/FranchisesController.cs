﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieFranchises.DTOs.CharacterDTOs;
using MovieFranchises.DTOs.FranchiseDTOs;
using MovieFranchises.DTOs.MovieDTOs;
using MovieFranchises.Models;
using MovieFranchises.Models.Context;

namespace MovieFranchises.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        /// <summary>
        /// Gets all franchises from the db
        /// </summary>
        /// <returns></returns>        
        /// <response code="200">Successfully returns franchise dto list object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _context.Franchises.ToListAsync();
            var dtoFranchises = _mapper.Map<List<FranchiseReadDTO>>(franchises);
            return dtoFranchises;
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Get a franchise based on Id
        /// </summary>
        /// <param name="id">The id of the franchise to get</param>
        /// <returns>a franchise dto object</returns>
        /// <response code="200">Successfully returns franchise dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            var franchiseDto = _mapper.Map<FranchiseReadDTO>(franchise);

            return franchiseDto;
        }

        // GET: api/Franchises/5/movies
        /// <summary>
        /// Get all movies from a franchise based on id
        /// </summary>
        /// <param name="id">Id of the franchise which movies to get</param>
        /// <returns>a franchises dto object containing a collection of movie dto objects</returns>
        /// <response code="200">Successfully returns franchise dto object containing movie dto objects</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<FranchiseReadMoviesDTO>> GetFranchiseMovies(int id)
        {
            var movies = await _context.Franchises.Where(f => f.Id == id).SelectMany(f => f.Movies).ToListAsync();

            if (movies == null)
            {
                return NotFound();
            }

            var franchiseDto = new FranchiseReadMoviesDTO();
            franchiseDto.Id = id;
            franchiseDto.MovieReadDTOs = _mapper.Map<List<MovieReadDTO>>(movies);

            return franchiseDto;
        }

        // GET: api/Franchises/5/characters
        /// <summary>
        /// Get all characters from a single franchise, may contain duplicates
        /// </summary>
        /// <param name="id">id of the franchise which characters to retrieve</param>
        /// <returns>a franchise dto object containing a collection of character dto objects</returns>
        /// <response code="200">Successfully returns franchise dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<FranchiseReadCharactersDTO>> GetFranchiseCharacters(int id)
        {
            var characters = await _context.Franchises.Where(f => f.Id == id).SelectMany(f => f.Movies).SelectMany(m=>m.Characters).ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }

            var franchiseDto = new FranchiseReadCharactersDTO();
            franchiseDto.Id = id;
            franchiseDto.CharacterReadDTOs = _mapper.Map<List<CharacterReadDTO>>(characters);

            return franchiseDto;
        }

        // PUT: api/Franchises/5
        /// <summary>
        /// Update a franchise based on id. Body must contain a franchise dto object
        /// </summary>
        /// <param name="id">the id of the franchise to update</param>
        /// <param name="franchiseDto">dto object containing new values of the franchise</param>
        /// <returns>a franchise dto object containing the new values</returns>
        /// <response code="200">Successfully returns franchise dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> PutFranchise(int id, FranchiseUpdateDTO franchiseDto)
        {
            if (id != franchiseDto.Id)
            {
                return BadRequest();
            }

            var newFranchise = _mapper.Map<Franchise>(franchiseDto);
            var oldFranchise = _context.Franchises.Find(id);

            try
            {
                if(oldFranchise!= null)
                {
                    oldFranchise.Name = newFranchise.Name;
                    oldFranchise.Description = newFranchise.Description;
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            FranchiseReadDTO newFranchiseDto = _mapper.Map<FranchiseReadDTO>(newFranchise);

            return newFranchiseDto;
        }

        // PUT: Api/Franchises/5/updatemovies
        /// <summary>
        /// Update a franchise with additional movies
        /// </summary>
        /// <param name="id">id of the franchise to update</param>
        /// <param name="franchiseDto">franchise dto object containg the ids of the movies to add</param>
        /// <returns>an updated franchise dto object</returns>
        /// <response code="200">Successfully returns franchise dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}/updatemovies")]
        public async Task<ActionResult<FranchiseReadDTO>> PutFranchiseMovies(int id, FranchiseUpdateMoviesDTO franchiseDto)
        {
            if (id != franchiseDto.Id)
            {
                return BadRequest();
            }

            Franchise oldFranchise = _context.Franchises.Find(id);

            try
            {
                if (oldFranchise != null)
                {
                    foreach (int movieId in franchiseDto.MovieIds)
                    {
                        var movie = _context.Movies.Find(movieId);
                        oldFranchise.Movies = await _context.Franchises.Where(f => f.Id == id).SelectMany(f => f.Movies).ToListAsync();
                        oldFranchise.Movies.Add(movie);
                    }
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            FranchiseReadDTO newFranchiseDto = _mapper.Map<FranchiseReadDTO>(oldFranchise);

            return newFranchiseDto;
        }


        // POST: api/Franchises
        /// <summary>
        /// Create a new franchise in the db without any relations
        /// </summary>
        /// <param name="franchiseDto">franchise dto object containing the values for the new franchise</param>
        /// <returns>the created franchise dto object</returns>
        /// <response code="200">Successfully returns franchise dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseDto)
        {
            var franchise = _mapper.Map<Franchise>(franchiseDto);
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            FranchiseReadDTO newFranchiseDto = _mapper.Map<FranchiseReadDTO>(franchise);

            return CreatedAtAction("GetFranchise", new { id = newFranchiseDto.Id }, newFranchiseDto);
        }

        // DELETE: api/Franchises/5
        /// <summary>
        /// Delete a franchise based on id
        /// </summary>
        /// <param name="id">id of the franchise to remove</param>
        /// <returns>a 204 no content message</returns>
        /// <response code="204">Successfully deleted franchise</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
