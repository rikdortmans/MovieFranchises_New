﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieFranchises.DTOs.CharacterDTOs;
using MovieFranchises.DTOs.MovieDTOs;
using MovieFranchises.Models;
using MovieFranchises.Models.Context;

namespace MovieFranchises.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        /// <summary>
        /// Retrieve all movies from the db
        /// </summary>
        /// <returns>a collection of movie dto objects</returns>
        /// <response code="200">Successfully returns movie dto list object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movies = await _context.Movies.ToListAsync();
            var moviesDto = _mapper.Map<List<MovieReadDTO>>(movies);
            return moviesDto;
        }

        /// <summary>
        /// Retrive a movie based on id from the db
        /// </summary>
        /// <param name="id">id of the movie to retrive</param>
        /// <returns>a movie dto object</returns>
        /// <response code="200">Successfully returns movie dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            var movieDto = _mapper.Map<MovieReadDTO>(movie);
            return movieDto;
        }

        // GET: api/Movies/5/characters
        /// <summary>
        /// Retrieve all characters from a movie based on movie id
        /// </summary>
        /// <param name="id">id of the movie which characters to retrive</param>
        /// <returns>a movie dto object containing a collection of character dto objects</returns>
        /// <response code="200">Successfully returns movie dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<MovieReadCharactersDTO>> GetMovieCharacters(int id)
        {
            var characters = await _context.Movies.Where(m => m.Id == id).SelectMany(m => m.Characters).ToListAsync();

            if (characters == null)
            {
                return NotFound();
            }


            var movieDto = new MovieReadCharactersDTO();
            movieDto.Id = id;
            movieDto.CharacterReadDTOs = _mapper.Map<List<CharacterReadDTO>>(characters);
            return movieDto;
        }

        // PUT: api/Movies/5
        /// <summary>
        /// Update movie based on id
        /// </summary>
        /// <param name="id">id of the movie to update</param>
        /// <param name="movie">dto object containing updated values</param>
        /// <returns>a dto object with the updated values</returns>
        /// <response code="200">Successfully returns movie dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<ActionResult<MovieReadDTO>> PutMovie(int id, MovieUpdateDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            var newMovie = _mapper.Map<Movie>(movie);
            var oldMovie = _context.Movies.Find(id);


            try
            {
                if (oldMovie != null)
                {
                    oldMovie.Title = newMovie.Title;
                    oldMovie.Genre = newMovie.Genre;
                    oldMovie.ReleaseYear = newMovie.ReleaseYear;
                    oldMovie.Director = newMovie.Director;
                    oldMovie.Poster = newMovie.Poster;
                    oldMovie.Trailer = newMovie.Trailer;
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            MovieReadDTO newMovieReadDto = _mapper.Map<MovieReadDTO>(newMovie);

            return newMovieReadDto;
        }

        // Put: api/Movies/5/updatecharacters
        /// <summary>
        /// Update the characters of a movie based on id
        /// </summary>
        /// <param name="id">id of the movie to update</param>
        /// <param name="movieDto">dto object containing the ids of the characters to add</param>
        /// <returns>a movie dto with the updated values</returns>
        /// <response code="200">Successfully returns movie dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}/updatecharacters")]
        public async Task<ActionResult<MovieReadDTO>> PutMovieCharacters(int id, MovieUpdateCharactersDTO movieDto)
        {
            if (id != movieDto.Id)
            {
                return BadRequest();
            }

            var oldMovie = _context.Movies.Find(id);
            

            try
            {
                if (oldMovie != null)
                {
                    foreach (int characterId in movieDto.CharacterIds)
                    {
                        var character = _context.Characters.Find(characterId);
                        oldMovie.Characters = await _context.Movies.Where(m => m.Id == id).SelectMany(m => m.Characters).ToListAsync();
                        oldMovie.Characters.Add(character);
                    }
                }
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            MovieReadDTO newMovieReadDto = _mapper.Map<MovieReadDTO>(oldMovie);

            return newMovieReadDto;
        }

        // POST: api/Movies
        /// <summary>
        /// Add a movie to the db
        /// </summary>
        /// <param name="movieDto">dto object containing the movie to add</param>
        /// <returns>a dto object of the added movie</returns>
        /// <response code="200">Successfully returns movie dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieDto)
        {
            var movie = _mapper.Map<Movie>(movieDto);
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            MovieReadDTO newMovieDto = _mapper.Map<MovieReadDTO>(movie);

            return CreatedAtAction("GetMovie", new { id = newMovieDto.Id }, newMovieDto);
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete a movie from the db based on id
        /// </summary>
        /// <param name="id">id of the movie to delete</param>
        /// <returns>a 204 no content messsage</returns>
        /// <response code="204">Successfully deleted movie</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
