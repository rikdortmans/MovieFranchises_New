﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieFranchises.DTOs.CharacterDTOs;
using MovieFranchises.Models;
using MovieFranchises.Models.Context;

namespace MovieFranchises.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets all characters from the Db without movies
        /// </summary>
        /// <returns>A list of character DTO objects</returns>
        ///<response code="200">Successfully returns character dto list object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {

            var characters = await _context.Characters.ToListAsync();

            var dtoCharacters = _mapper.Map<List<CharacterReadDTO>>(characters);
            return dtoCharacters;
        }

        // GET: api/Characters/5
        /// <summary>
        /// Gets a character from DB by ID.
        /// </summary>
        /// <param name="id">Character to get</param>
        /// <returns>A character Dto object</returns>
        /// <response code="200">Successfully returns character dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            var characterDto = _mapper.Map<CharacterReadDTO>(character);

            return characterDto;
        }

        // PUT: api/Characters/5
        /// <summary>
        /// Updates a character in the Db based on Id
        /// </summary>
        /// <param name="id">Id of the character that needs updating</param>
        /// <param name="characterDto">The character DTO object</param>
        /// <returns>The updated character as dto object</returns>
        /// <response code="200">Successfully returns character dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> PutCharacter(int id, CharacterUpdateDTO characterDto)
        {
            if (id != characterDto.Id)
            {
                return BadRequest();
            }

            var newCharacter = _mapper.Map<Character>(characterDto);
            var oldCharacter = _context.Characters.Find(id);

            try
            {
                if (oldCharacter != null)
                {
                    oldCharacter.Name = newCharacter.Name;
                    oldCharacter.Alias = newCharacter.Alias;
                    oldCharacter.Gender = newCharacter.Gender;
                    oldCharacter.Picture = newCharacter.Picture;
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            CharacterReadDTO newCharacterDto = _mapper.Map<CharacterReadDTO>(newCharacter);

            return newCharacterDto;
        }

        // POST: api/Characters
        /// <summary>
        /// Create a new character in the Db
        /// </summary>
        /// <param name="characterDto">The character object to create as dto object</param>
        /// <returns>The created character as dto object</returns>
        /// <response code="200">Successfully returns character dto object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO characterDto)
        {
            var character = _mapper.Map<Character>(characterDto);

            _context.Characters.Add(character);
            await _context.SaveChangesAsync();

            CharacterReadDTO newCharacterDto = _mapper.Map<CharacterReadDTO>(character);

            return CreatedAtAction("GetCharacter", new { id = newCharacterDto.Id }, newCharacterDto);
        }

        // DELETE: api/Characters/5
        /// <summary>
        /// Delete a character from the db based on id
        /// </summary>
        /// <param name="id">id of the character to delete</param>
        /// <returns>No content result</returns>
        ///<response code="204">Successfully deleted character</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
