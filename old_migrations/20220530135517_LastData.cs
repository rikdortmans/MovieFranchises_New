﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieFranchises.Migrations
{
    public partial class LastData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786", "https://www.youtube.com/watch?v=6Oebyc--Tzc" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786", "https://www.youtube.com/watch?v=6Oebyc--Tzc" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397", "https://www.youtube.com/watch?v=I5EFrZZgxh8" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397", "https://www.youtube.com/watch?v=I5EFrZZgxh8" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931", "https://www.youtube.com/watch?v=B9V0bOB8sXQ" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931", "https://www.youtube.com/watch?v=B9V0bOB8sXQ" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "", "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Poster", "Trailer" },
                values: new object[] { "", "" });
        }
    }
}
