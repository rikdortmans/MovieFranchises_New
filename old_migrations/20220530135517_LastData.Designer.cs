﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieFranchises.Models.Context;

#nullable disable

namespace MovieFranchises.Migrations
{
    [DbContext(typeof(MovieDbContext))]
    [Migration("20220530135517_LastData")]
    partial class LastData
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("CharactersId")
                        .HasColumnType("int");

                    b.Property<int>("MoviesId")
                        .HasColumnType("int");

                    b.HasKey("CharactersId", "MoviesId");

                    b.HasIndex("MoviesId");

                    b.ToTable("CharacterMovie");

                    b.HasData(
                        new
                        {
                            CharactersId = 1,
                            MoviesId = 1
                        },
                        new
                        {
                            CharactersId = 1,
                            MoviesId = 2
                        },
                        new
                        {
                            CharactersId = 1,
                            MoviesId = 4
                        },
                        new
                        {
                            CharactersId = 2,
                            MoviesId = 2
                        },
                        new
                        {
                            CharactersId = 2,
                            MoviesId = 3
                        },
                        new
                        {
                            CharactersId = 3,
                            MoviesId = 2
                        },
                        new
                        {
                            CharactersId = 3,
                            MoviesId = 3
                        },
                        new
                        {
                            CharactersId = 4,
                            MoviesId = 3
                        },
                        new
                        {
                            CharactersId = 4,
                            MoviesId = 4
                        },
                        new
                        {
                            CharactersId = 5,
                            MoviesId = 5
                        },
                        new
                        {
                            CharactersId = 5,
                            MoviesId = 6
                        },
                        new
                        {
                            CharactersId = 6,
                            MoviesId = 5
                        },
                        new
                        {
                            CharactersId = 6,
                            MoviesId = 6
                        });
                });

            modelBuilder.Entity("MovieFranchises.Models.Character", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Alias")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Gender")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Characters");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Alias = "The Monk",
                            Gender = "Male",
                            Name = "Sognir",
                            Picture = ""
                        },
                        new
                        {
                            Id = 2,
                            Alias = "The Druid",
                            Gender = "Female",
                            Name = "Aunrae-Oyi",
                            Picture = ""
                        },
                        new
                        {
                            Id = 3,
                            Alias = "The Gunslinger",
                            Gender = "Male",
                            Name = "Akim",
                            Picture = ""
                        },
                        new
                        {
                            Id = 4,
                            Alias = "The Ranger",
                            Gender = "Female",
                            Name = "Shaxana",
                            Picture = ""
                        },
                        new
                        {
                            Id = 5,
                            Alias = "The Librarian",
                            Gender = "Male",
                            Name = "Zahariel",
                            Picture = ""
                        },
                        new
                        {
                            Id = 6,
                            Alias = "The Vanguard",
                            Gender = "Male",
                            Name = "Flaen",
                            Picture = ""
                        });
                });

            modelBuilder.Entity("MovieFranchises.Models.Franchise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Franchises");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Dungeons & Dragons 5e",
                            Name = "Age of Light"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Warhammer 40k",
                            Name = "Deathwatch"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Pathfinder",
                            Name = "Pond of Life"
                        });
                });

            modelBuilder.Entity("MovieFranchises.Models.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("Director")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Poster")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ReleaseYear")
                        .HasColumnType("int");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Trailer")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movies");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Director = "Rik",
                            FranchiseId = 3,
                            Genre = "Adventure",
                            Poster = "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786",
                            ReleaseYear = 2018,
                            Title = "The Ruined City",
                            Trailer = "https://www.youtube.com/watch?v=6Oebyc--Tzc"
                        },
                        new
                        {
                            Id = 2,
                            Director = "Rik",
                            FranchiseId = 1,
                            Genre = "Adventure / War",
                            Poster = "https://s3.amazonaws.com/files.d20.io/images/27182988/TPJi-cbeSXAVL9zK-XKIsA/med.jpg?1483990661786",
                            ReleaseYear = 2019,
                            Title = "The Pandomoneum",
                            Trailer = "https://www.youtube.com/watch?v=6Oebyc--Tzc"
                        },
                        new
                        {
                            Id = 3,
                            Director = "Daniel",
                            FranchiseId = 1,
                            Genre = "Horror / War",
                            Poster = "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397",
                            ReleaseYear = 2020,
                            Title = "The Citadel",
                            Trailer = "https://www.youtube.com/watch?v=I5EFrZZgxh8"
                        },
                        new
                        {
                            Id = 4,
                            Director = "Daniel",
                            FranchiseId = 3,
                            Genre = "Detective / Adventure",
                            Poster = "https://s3.amazonaws.com/files.d20.io/images/265507366/OJ7o4hZdIu-cVLkeOqNMDQ/med.jpg?1642421233397",
                            ReleaseYear = 2022,
                            Title = "The Temple of Time",
                            Trailer = "https://www.youtube.com/watch?v=I5EFrZZgxh8"
                        },
                        new
                        {
                            Id = 5,
                            Director = "Tim",
                            FranchiseId = 2,
                            Genre = "War / Horror",
                            Poster = "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931",
                            ReleaseYear = 2021,
                            Title = "Tyranids",
                            Trailer = "https://www.youtube.com/watch?v=B9V0bOB8sXQ"
                        },
                        new
                        {
                            Id = 6,
                            Director = "Tim",
                            FranchiseId = 2,
                            Genre = "War / Horror",
                            Poster = "https://s3.amazonaws.com/files.d20.io/images/238683514/gqoKZ6omyx4ZqKXsSYmdUQ/med.png?1628417421931",
                            ReleaseYear = 2021,
                            Title = "Necrons",
                            Trailer = "https://www.youtube.com/watch?v=B9V0bOB8sXQ"
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("MovieFranchises.Models.Character", null)
                        .WithMany()
                        .HasForeignKey("CharactersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieFranchises.Models.Movie", null)
                        .WithMany()
                        .HasForeignKey("MoviesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MovieFranchises.Models.Movie", b =>
                {
                    b.HasOne("MovieFranchises.Models.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("MovieFranchises.Models.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
