﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieFranchises.Migrations
{
    public partial class SeedSimpleData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 1, "Dungeons & Dragons 5e", "Age of Light" });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 2, "Warhammer 40k", "Deathwatch" });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { 3, "Pathfinder", "Pond of Life" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Poster", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Rik", 3, "Adventure", "", 2018, "The Ruined City", "" },
                    { 2, "Rik", 1, "Adventure / War", "", 2019, "The Pandomoneum", "" },
                    { 3, "Rik", 1, "Horror / War", "", 2020, "The Lord of Skulls", "" },
                    { 4, "Rik", 1, "Detective / Adventure", "", 2020, "Assassinations", "" },
                    { 5, "Tim", 2, "War / Horror", "", 2021, "Tyranids", "" },
                    { 6, "Tim", 2, "War / Horror", "", 2021, "Necrons", "" },
                    { 7, "Tim", 2, "War / Horror", "", 2021, "The Space Hulk", "" },
                    { 8, "Tim", 2, "War", "", 2022, "Steel Legion", "" },
                    { 9, "Tim", 2, "War", "", 2022, "A brother returns", "" },
                    { 10, "Daniel", 3, "Adventure / Horror", "", 2018, "The Citadel", "" },
                    { 11, "Daniel", 3, "Adventure", "", 2018, "The Captain", "" },
                    { 12, "Daniel", 3, "Adventure", "", 2019, "The Mountain Erupts", "" },
                    { 13, "Daniel", 1, "Adventure", "", 2020, "The God Beasts", "" },
                    { 14, "Daniel", 1, "Adventure / Puzzles", "", 2022, "The Temple of Time", "" },
                    { 15, "Daniel", 1, "Adventure / Puzzles", "", 2022, "The Ruins", "" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
