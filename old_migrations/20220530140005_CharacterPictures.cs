﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieFranchises.Migrations
{
    public partial class CharacterPictures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1,
                column: "Picture",
                value: "https://static.wikia.nocookie.net/forgottenrealms/images/7/79/Firbolg-5e.jpg/revision/latest?cb=20180623050202");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2,
                column: "Picture",
                value: "https://s3.amazonaws.com/files.d20.io/images/45807231/LNQ8zPoA6_Gf8VPaWjTEhg/med.png?1516141022");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3,
                column: "Picture",
                value: "https://s3.amazonaws.com/files.d20.io/images/84614442/5b3FdW_coX5_j5t5IzTPJw/med.png?1561215507");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4,
                column: "Picture",
                value: "https://s3.amazonaws.com/files.d20.io/images/267636873/kZthTiBD9XeRvZD_T7s4OA/med.png?1643399798");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5,
                column: "Picture",
                value: "https://s3.amazonaws.com/files.d20.io/images/250519342/nBiHdE4wJE5hCGgctwBPFg/med.jpg?1634325603");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 6,
                column: "Picture",
                value: "https://s3.amazonaws.com/files.d20.io/images/266766963/YPcQ94CBhtMtmtrzAq0mCg/med.jpg?1642966999");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1,
                column: "Picture",
                value: "");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2,
                column: "Picture",
                value: "");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3,
                column: "Picture",
                value: "");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4,
                column: "Picture",
                value: "");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5,
                column: "Picture",
                value: "");

            migrationBuilder.UpdateData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 6,
                column: "Picture",
                value: "");
        }
    }
}
